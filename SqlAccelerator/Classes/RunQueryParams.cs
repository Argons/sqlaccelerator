﻿using SqlAccelerator.Classes;
using System.Collections.Generic;

namespace SqlAccelerator
{
    public class RunQueryParams
    {
        public string SqlQuery { get; set; }
        public List<ListItem> ColConnections { get; set; }
    }
}
