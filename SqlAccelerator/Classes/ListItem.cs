﻿
namespace SqlAccelerator.Classes
{
    public class ListItem
    {
        public byte Id { get; set; }
        public object Value { get; set; }
        public string Text { get; set; }

        public ListItem(object value, string text)
        {
            Value = value;
            Text = text;
        }

        public ListItem(byte id, object value, string text)
        {
            Value = value;
            Text = text;
            Id = id;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
