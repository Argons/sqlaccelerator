﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SqlAccelerator
{
    public partial class AddConnection : Form
    {
        public AddConnection()
        {
            InitializeComponent();
        }

        public string ConnectionString { get; set; }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder(txtConnectionString.Text);
                ConnectionString = connBuilder.ConnectionString;
                DialogResult = System.Windows.Forms.DialogResult.OK;
                txtConnectionString.Text = "";
                Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show("There was an error trying to add the new connection.\n" + exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            txtConnectionString.Text = "";
            Close();
        }
    }
}
