﻿namespace SqlAccelerator
{
    partial class ConnectionsToolWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectionsToolWindow));
            this.chklstConnections = new System.Windows.Forms.CheckedListBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbtnNewList = new System.Windows.Forms.ToolStripButton();
            this.tsbtnOpenList = new System.Windows.Forms.ToolStripButton();
            this.tsbtnSaveList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnAddConnection = new System.Windows.Forms.ToolStripButton();
            this.tsbtnRemoveConnection = new System.Windows.Forms.ToolStripButton();
            this.ofdList = new System.Windows.Forms.OpenFileDialog();
            this.sfdList = new System.Windows.Forms.SaveFileDialog();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chklstConnections
            // 
            this.chklstConnections.CheckOnClick = true;
            this.chklstConnections.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chklstConnections.Location = new System.Drawing.Point(0, 25);
            this.chklstConnections.Name = "chklstConnections";
            this.chklstConnections.Size = new System.Drawing.Size(250, 237);
            this.chklstConnections.Sorted = true;
            this.chklstConnections.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnNewList,
            this.tsbtnOpenList,
            this.tsbtnSaveList,
            this.toolStripSeparator1,
            this.tsbtnAddConnection,
            this.tsbtnRemoveConnection});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(250, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbtnNewList
            // 
            this.tsbtnNewList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnNewList.Image = global::SqlAccelerator.Properties.Resources.add_property_26;
            this.tsbtnNewList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnNewList.Name = "tsbtnNewList";
            this.tsbtnNewList.Size = new System.Drawing.Size(23, 22);
            this.tsbtnNewList.Text = "toolStripButton1";
            this.tsbtnNewList.ToolTipText = "New Connection List";
            this.tsbtnNewList.Click += new System.EventHandler(this.tsbtnNewList_Click);
            // 
            // tsbtnOpenList
            // 
            this.tsbtnOpenList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnOpenList.Image = global::SqlAccelerator.Properties.Resources.folder_32;
            this.tsbtnOpenList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnOpenList.Name = "tsbtnOpenList";
            this.tsbtnOpenList.Size = new System.Drawing.Size(23, 22);
            this.tsbtnOpenList.Text = "Open List";
            this.tsbtnOpenList.Click += new System.EventHandler(this.tsbtnOpenList_Click);
            // 
            // tsbtnSaveList
            // 
            this.tsbtnSaveList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnSaveList.Image = global::SqlAccelerator.Properties.Resources.save_26;
            this.tsbtnSaveList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnSaveList.Name = "tsbtnSaveList";
            this.tsbtnSaveList.Size = new System.Drawing.Size(23, 22);
            this.tsbtnSaveList.Text = "New Connection List";
            this.tsbtnSaveList.ToolTipText = "Save Connection List";
            this.tsbtnSaveList.Click += new System.EventHandler(this.tsbtnSaveList_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnAddConnection
            // 
            this.tsbtnAddConnection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnAddConnection.Image = global::SqlAccelerator.Properties.Resources.add_database_26;
            this.tsbtnAddConnection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnAddConnection.Name = "tsbtnAddConnection";
            this.tsbtnAddConnection.Size = new System.Drawing.Size(23, 22);
            this.tsbtnAddConnection.Text = "toolStripButton2";
            this.tsbtnAddConnection.ToolTipText = "Add Connection";
            this.tsbtnAddConnection.Click += new System.EventHandler(this.tsbtnAddConnection_Click);
            // 
            // tsbtnRemoveConnection
            // 
            this.tsbtnRemoveConnection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnRemoveConnection.Image = global::SqlAccelerator.Properties.Resources.delete_database_26;
            this.tsbtnRemoveConnection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnRemoveConnection.Name = "tsbtnRemoveConnection";
            this.tsbtnRemoveConnection.Size = new System.Drawing.Size(23, 22);
            this.tsbtnRemoveConnection.Text = "toolStripButton4";
            this.tsbtnRemoveConnection.ToolTipText = "Remove Connection";
            this.tsbtnRemoveConnection.Click += new System.EventHandler(this.tsbtnRemoveConnection_Click);
            // 
            // ofdList
            // 
            this.ofdList.Filter = "Connection files|*.txt";
            this.ofdList.Title = "Open Connection List";
            // 
            // sfdList
            // 
            this.sfdList.Filter = "Connection files|*.txt";
            this.sfdList.Title = "Save Connection List";
            // 
            // ConnectionsToolWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 262);
            this.Controls.Add(this.chklstConnections);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConnectionsToolWindow";
            this.Text = "Connections";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox chklstConnections;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbtnNewList;
        private System.Windows.Forms.ToolStripButton tsbtnAddConnection;
        private System.Windows.Forms.ToolStripButton tsbtnSaveList;
        private System.Windows.Forms.ToolStripButton tsbtnRemoveConnection;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbtnOpenList;
        private System.Windows.Forms.OpenFileDialog ofdList;
        private System.Windows.Forms.SaveFileDialog sfdList;
    }
}