﻿using SqlAccelerator.Classes;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SqlAccelerator
{
    public partial class Shell : Form
    {
        #region " Fields "

        private ConnectionsToolWindow connToolWindow = new ConnectionsToolWindow();
        private int queryCount = 1;

        #endregion

        #region " Properties "

        public List<ListItem> SelectedConnections
        {
            get { return connToolWindow.SelectedConnections; }
        }

        #endregion


        public Shell()
        {
            InitializeComponent();
        }

        private void Shell_Load(object sender, EventArgs e)
        {
            dockPanel1.SuspendLayout(true);
            connToolWindow.Show(dockPanel1, WeifenLuo.WinFormsUI.Docking.DockState.DockLeft);
            dockPanel1.ResumeLayout(true, true);
        }

        private void tsbtnExecute_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild != null && ActiveMdiChild is QueryManager)
            {
                ((QueryManager)ActiveMdiChild).RunQuery(SelectedConnections);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.R))
            {
                if (ActiveMdiChild != null && ActiveMdiChild is QueryManager)
                    ((QueryManager)ActiveMdiChild).HideShowResults();
                return true;
            }
            else
            {
                switch (keyData)
                {
                    case Keys.F5:
                        tsbtnExecute_Click(null, EventArgs.Empty);
                        return true;
                    case Keys.F6:
                        tsbtnStop_Click(null, EventArgs.Empty);
                        return true;
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void tsbtnStop_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild != null && ActiveMdiChild is QueryManager)
            {
                if (((QueryManager)ActiveMdiChild).RunningQuery)
                    ((QueryManager)ActiveMdiChild).CancelQuery();
            }
        }

        private void tsbtnNew_Click(object sender, EventArgs e)
        {
            dockPanel1.SuspendLayout(true);
            QueryManager query = new QueryManager();
            query.Text = "Query " + queryCount++;
            query.Show(dockPanel1, DockState.Document);
            dockPanel1.ResumeLayout(true, true);
        }

        private void tsbtnOpen_Click(object sender, EventArgs e)
        {
            if (ofdQueryOpen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                dockPanel1.SuspendLayout(true);
                QueryManager query = new QueryManager();
                query.OpenFile(ofdQueryOpen.FileName);
                query.Show(dockPanel1, DockState.Document);
                dockPanel1.ResumeLayout(true, true);
            }
        }

        private void tsbtnSave_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild != null && ActiveMdiChild is QueryManager)
            {
                var frmQuery = (QueryManager)ActiveMdiChild;
                if (!string.IsNullOrWhiteSpace(frmQuery.FilePath))
                {
                    frmQuery.SaveFile();
                }
                else
                {
                    if (sfdQuerySave.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        frmQuery.SaveFile(sfdQuerySave.FileName);
                }
            }
        }

    }
}
