﻿namespace SqlAccelerator
{
    partial class QueryManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueryManager));
      this.sqlEditor = new ScintillaNET.Scintilla();
      this.spliterQuery = new System.Windows.Forms.SplitContainer();
      this.tabResults = new System.Windows.Forms.TabControl();
      this.statusQuery = new System.Windows.Forms.StatusStrip();
      this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.statusTime = new System.Windows.Forms.ToolStripStatusLabel();
      this.backgrounRunQuery = new System.ComponentModel.BackgroundWorker();
      this.timerRunQuery = new System.Windows.Forms.Timer(this.components);
      ((System.ComponentModel.ISupportInitialize)(this.sqlEditor)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.spliterQuery)).BeginInit();
      this.spliterQuery.SuspendLayout();
      this.statusQuery.SuspendLayout();
      this.SuspendLayout();
      // 
      // sqlEditor
      // 
      this.sqlEditor.ConfigurationManager.Language = "mssql";
      this.sqlEditor.Dock = System.Windows.Forms.DockStyle.Fill;
      this.sqlEditor.Indentation.ShowGuides = true;
      this.sqlEditor.Indentation.TabWidth = 4;
      this.sqlEditor.Indentation.UseTabs = false;
      this.sqlEditor.Lexing.Lexer = ScintillaNET.Lexer.MsSql;
      this.sqlEditor.Lexing.LexerName = "mssql";
      this.sqlEditor.Lexing.LineCommentPrefix = "";
      this.sqlEditor.Lexing.StreamCommentPrefix = "";
      this.sqlEditor.Lexing.StreamCommentSufix = "";
      this.sqlEditor.Location = new System.Drawing.Point(0, 0);
      this.sqlEditor.Margins.Margin0.IsFoldMargin = true;
      this.sqlEditor.Margins.Margin0.Width = 40;
      this.sqlEditor.Name = "sqlEditor";
      this.sqlEditor.Size = new System.Drawing.Size(624, 225);
      this.sqlEditor.Styles.BraceBad.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.Styles.BraceLight.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.Styles.CallTip.FontName = "Segoe UI\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.Styles.ControlChar.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.Styles.Default.BackColor = System.Drawing.SystemColors.Window;
      this.sqlEditor.Styles.Default.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.Styles.IndentGuide.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.Styles.LastPredefined.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.Styles.LineNumber.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.Styles.Max.FontName = "Verdana\0\0\0\0\0\0\0\0\0\0\0\0\0";
      this.sqlEditor.TabIndex = 0;
      // 
      // spliterQuery
      // 
      this.spliterQuery.Cursor = System.Windows.Forms.Cursors.Default;
      this.spliterQuery.Dock = System.Windows.Forms.DockStyle.Fill;
      this.spliterQuery.Location = new System.Drawing.Point(0, 0);
      this.spliterQuery.Name = "spliterQuery";
      this.spliterQuery.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // spliterQuery.Panel1
      // 
      this.spliterQuery.Panel1.Controls.Add(this.sqlEditor);
      // 
      // spliterQuery.Panel2
      // 
      this.spliterQuery.Panel2.Controls.Add(this.tabResults);
      this.spliterQuery.Size = new System.Drawing.Size(624, 413);
      this.spliterQuery.SplitterDistance = 225;
      this.spliterQuery.TabIndex = 1;
      this.spliterQuery.SplitterMoving += new System.Windows.Forms.SplitterCancelEventHandler(this.kryptonSplitContainer1_SplitterMoving);
      this.spliterQuery.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.kryptonSplitContainer1_SplitterMoved);
      // 
      // tabResults
      // 
      this.tabResults.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabResults.Location = new System.Drawing.Point(0, 0);
      this.tabResults.Name = "tabResults";
      this.tabResults.SelectedIndex = 0;
      this.tabResults.Size = new System.Drawing.Size(624, 184);
      this.tabResults.TabIndex = 0;
      // 
      // statusQuery
      // 
      this.statusQuery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.statusTime});
      this.statusQuery.Location = new System.Drawing.Point(0, 413);
      this.statusQuery.Name = "statusQuery";
      this.statusQuery.Size = new System.Drawing.Size(624, 22);
      this.statusQuery.SizingGrip = false;
      this.statusQuery.TabIndex = 2;
      this.statusQuery.Text = "statusStrip1";
      // 
      // statusLabel
      // 
      this.statusLabel.Name = "statusLabel";
      this.statusLabel.Size = new System.Drawing.Size(499, 17);
      this.statusLabel.Spring = true;
      this.statusLabel.Text = "Ready";
      this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // statusTime
      // 
      this.statusTime.AutoSize = false;
      this.statusTime.Name = "statusTime";
      this.statusTime.Size = new System.Drawing.Size(110, 17);
      // 
      // backgrounRunQuery
      // 
      this.backgrounRunQuery.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgrounRunQuery_DoWork);
      this.backgrounRunQuery.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgrounRunQuery_RunWorkerCompleted);
      // 
      // timerRunQuery
      // 
      this.timerRunQuery.Tick += new System.EventHandler(this.timerRunQuery_Tick);
      // 
      // QueryManager
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(624, 435);
      this.Controls.Add(this.spliterQuery);
      this.Controls.Add(this.statusQuery);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "QueryManager";
      this.Text = "Query";
      ((System.ComponentModel.ISupportInitialize)(this.sqlEditor)).EndInit();
      this.spliterQuery.Panel1.ResumeLayout(false);
      this.spliterQuery.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.spliterQuery)).EndInit();
      this.spliterQuery.ResumeLayout(false);
      this.statusQuery.ResumeLayout(false);
      this.statusQuery.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private ScintillaNET.Scintilla sqlEditor;
        private System.Windows.Forms.SplitContainer spliterQuery;
        private System.Windows.Forms.TabControl tabResults;
        private System.Windows.Forms.StatusStrip statusQuery;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.ComponentModel.BackgroundWorker backgrounRunQuery;
        private System.Windows.Forms.Timer timerRunQuery;
        private System.Windows.Forms.ToolStripStatusLabel statusTime;



    }
}