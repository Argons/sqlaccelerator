﻿using SqlAccelerator.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SqlAccelerator
{
    public partial class ConnectionsToolWindow : DockContent
    {
        #region " Fields "

        private string currentList = "";
        private HashSet<string> connectionList = new HashSet<string>();
        private List<ListItem> selectedConnections = new List<ListItem>();

        private AddConnection addConn = new AddConnection();

        #endregion

        #region " Properties "

        public List<ListItem> SelectedConnections
        {
            get
            {
                selectedConnections.Clear();
                foreach (ListItem item in chklstConnections.CheckedItems)
                    selectedConnections.Add(item);

                return selectedConnections;
            }
        }

        #endregion

        public ConnectionsToolWindow()
        {
            InitializeComponent();
        }

        private void tsbtnNewList_Click(object sender, EventArgs e)
        {
            connectionList.Clear();
            currentList = "";
            chklstConnections.Items.Clear();
            Text = "Connections";
        }

        private void tsbtnOpenList_Click(object sender, EventArgs e)
        {
            if (ofdList.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tsbtnNewList_Click(sender, e);
                currentList = ofdList.FileName;
                Text = "Connections - " + Path.GetFileNameWithoutExtension(currentList);
                LoadConnections();
            }
        }

        private void LoadConnections()
        {
            SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();
            using (StreamReader connReader = new StreamReader(currentList))
            {
                string connLine;
                while ((connLine = connReader.ReadLine()) != null)
                {
                    connectionList.Add(connLine);
                    connBuilder.ConnectionString = connLine;
                    chklstConnections.Items.Add(new ListItem((byte)chklstConnections.Items.Count, connLine, connBuilder.DataSource + " [" + connBuilder.InitialCatalog + "]"));
                }
            }
        }

        private void tsbtnSaveList_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentList != "")
                {
                    using (StreamWriter writer = new StreamWriter(currentList))
                    {
                        foreach (ListItem conn in chklstConnections.Items)
                            writer.WriteLine(conn.Value);
                    }
                }
                else
                {
                    if (sfdList.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (StreamWriter writer = new StreamWriter(sfdList.FileName))
                        {
                            foreach (ListItem conn in chklstConnections.Items)
                                writer.WriteLine(conn.Value);
                        }
                        currentList = sfdList.FileName;
                        Text = "Connections - " + Path.GetFileNameWithoutExtension(sfdList.FileName); ;
                    }
                }

                MessageBox.Show("The list was saved successfully.");
            }
            catch (Exception exc)
            {
                MessageBox.Show("An error ocurred while trying to save the list.\n" + exc.Message);
            }
        }

        private void tsbtnAddConnection_Click(object sender, EventArgs e)
        {
            addConn.ConnectionString = "";
            if (addConn.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();
                connectionList.Add(addConn.ConnectionString);
                connBuilder.ConnectionString = addConn.ConnectionString;
                chklstConnections.Items.Add(new ListItem((byte)chklstConnections.Items.Count, addConn.ConnectionString, connBuilder.DataSource + " [" + connBuilder.InitialCatalog + "]"));
            }
        }

        private void tsbtnRemoveConnection_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to remove the selected connections?", "Remove Connections", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                ListItem[] itemsToDel = new ListItem[SelectedConnections.Count];
                SelectedConnections.CopyTo(itemsToDel);
                foreach (var conn in itemsToDel)
                {
                    connectionList.Remove(conn.Value.ToString());
                    chklstConnections.Items.Remove(conn);
                }
            }
        }
    }
}
