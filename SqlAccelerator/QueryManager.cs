﻿using SqlAccelerator.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SqlAccelerator
{
    public partial class QueryManager : DockContent
    {
        #region " Fields "

        private TimeSpan timeExecution = new TimeSpan();
        private DataSet ds = new DataSet();
        private bool cancelQuery = false;

        #endregion

        #region " Properties "

        public bool RunningQuery { get; set; }
        public string FilePath { get; set; }

        #endregion

        #region " Constructor "

        public QueryManager()
        {
            InitializeComponent();
            HideShowResults();
        }

        #endregion

        #region " Form Events "

        private void kryptonSplitContainer1_SplitterMoving(object sender, SplitterCancelEventArgs e)
        {
            SuspendLayout();
        }

        private void kryptonSplitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            ResumeLayout();
        }

        private void timerRunQuery_Tick(object sender, EventArgs e)
        {
            statusTime.Text = (DateTime.Now.TimeOfDay - timeExecution).ToString(@"h\h\:m\m\:s\s", System.Globalization.CultureInfo.InvariantCulture);
        }

        #endregion

        #region " Query Related Methods "

        private void ClearResults()
        {
            foreach (TabPage tab in tabResults.TabPages)
                tab.Dispose();
            tabResults.TabPages.Clear();
            ds.Clear();
            ds.Tables.Clear();
        }

        public void RunQuery(List<ListItem> connectionList)
        {
            ClearResults();

            if (spliterQuery.Panel2Collapsed)
                HideShowResults();

            RunningQuery = true;
            timerRunQuery.Start();
            statusLabel.Text = "Running";
            backgrounRunQuery.RunWorkerAsync(new RunQueryParams
            {
                ColConnections = connectionList,
                SqlQuery = (string.IsNullOrEmpty(sqlEditor.Selection.Text)) ? sqlEditor.Text : sqlEditor.Selection.Text
            });
            timeExecution = DateTime.Now.TimeOfDay;
        }

        private TabPage[] InternalRunQuery(List<ListItem> connectionList, string sqlQuery)
        {
            TabPage[] colTabPages = new TabPage[connectionList.Count];
            int indexTabPage = 0;

            foreach (var conn in connectionList)
            {
                if (cancelQuery)
                {
                    cancelQuery = false;
                    colTabPages = null;
                    RunningQuery = false;
                    throw new Exception("Query cancelled.");
                }

                colTabPages[indexTabPage] = new TabPage(conn.Text);
                colTabPages[indexTabPage].AutoScroll = true;
                using (var sqlDA = new SqlDataAdapter(sqlQuery, conn.Value.ToString()))
                {
                    try
                    {
                        sqlDA.Fill(ds);
                        for (int index = ds.Tables.Count; index > 0; index--)
                        {
                            DataGridView grid = new DataGridView();
                            grid.DataSource = ds.Tables[index - 1].Copy();
                            grid.AllowUserToAddRows = false;
                            grid.AllowUserToDeleteRows = false;
                            grid.ReadOnly = true;
                            colTabPages[indexTabPage].Controls.Add(grid);
                            if (ds.Tables.Count == 1)
                                grid.Dock = DockStyle.Fill;
                            else
                                grid.Dock = DockStyle.Top;
                        }
                        ds.Clear();
                        ds.Tables.Clear();
                    }
                    catch (Exception exc)
                    {
                        TextBox labelError = new TextBox();
                        labelError.ScrollBars = ScrollBars.Vertical;
                        labelError.Multiline = true;
                        labelError.ReadOnly = true;
                        labelError.Text = exc.Message;
                        labelError.Dock = DockStyle.Fill;
                        colTabPages[indexTabPage].Controls.Add(labelError);
                    }
                }

                indexTabPage++;
            }

            return colTabPages;
        }

        private void backgrounRunQuery_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            RunQueryParams queryParams = (RunQueryParams)e.Argument;
            e.Result = InternalRunQuery(queryParams.ColConnections, queryParams.SqlQuery);
        }

        private void backgrounRunQuery_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            timerRunQuery.Stop();
            RunningQuery = false;
            statusTime.Text = (DateTime.Now.TimeOfDay - timeExecution).ToString(@"h\h\:m\m\:s\s", System.Globalization.CultureInfo.InvariantCulture);

            if (e.Error != null)
            {
                statusLabel.Text = e.Error.ToString();
            }
            else
            {
                statusLabel.Text = "Ready";
                tabResults.TabPages.AddRange((TabPage[])e.Result);
            }
        }

        public void CancelQuery()
        {
            cancelQuery = true;
        }

        #endregion

        #region " File Related Methods "

        public void OpenFile(string pathFile)
        {
            try
            {
                sqlEditor.Text = File.ReadAllText(pathFile);
                this.Text = Path.GetFileNameWithoutExtension(pathFile);
                FilePath = pathFile;
            }
            catch (Exception exc)
            {
                MessageBox.Show("An error ocurred while trying to load the file.\n" + exc.Message);
                this.Close();
            }
        }

        public void SaveFile(string pathFile)
        {
            try
            {
                File.WriteAllText(pathFile, sqlEditor.Text);
                this.Text = Path.GetFileNameWithoutExtension(pathFile);
                FilePath = pathFile;
                MessageBox.Show("The file was saved successfully.");
            }
            catch (Exception exc)
            {
                MessageBox.Show("An error ocurred while trying to save the file.\n" + exc.Message);
            }
        }

        public void SaveFile()
        {
            try
            {
                File.WriteAllText(FilePath, sqlEditor.Text);
                MessageBox.Show("The file was saved successfully.");
            }
            catch (Exception exc)
            {
                MessageBox.Show("An error ocurred while trying to save the file.\n" + exc.Message);
            }
        }

        #endregion

        #region " Misc Methods "

        public void HideShowResults()
        {
            spliterQuery.Panel2Collapsed = !spliterQuery.Panel2Collapsed;
            if (spliterQuery.Panel2Collapsed)
                spliterQuery.Panel2.Hide();
            else
                spliterQuery.Panel2.Show();
        }

        #endregion
    }
}
